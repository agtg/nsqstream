# RnaSeq tools and wrappers.

----
Source code used internally by our group on Compute Canada clusters.

It is provided as is under a permissive BSD 3 clause license. See LICENSE file.

Copyright (c) 2021  The authors of agtg/nsqstream.
Authors list can be retrieved from git commits.

