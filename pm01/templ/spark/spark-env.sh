#!/usr/bin/env bash

#export PYSPARK_PYTHON=/gs/project/wst-164-ab/share/raglab_prod/software/python/Python-3.5.1-inst/bin/python3
#export PYSPARK_DRIVER_PYTHON=/gs/project/wst-164-ab/share/raglab_prod/software/python/Python-3.5.1-inst/bin/ipython3 

export PYSPARK_PYTHON=python3
export PYSPARK_DRIVER_PYTHON=ipython3 

# SPARK_MASTER_IP
export SPARK_MASTER_HOST={{redis_host}}
export PROJ_DIR={{proj_dir}}
export SPARK_LOC_CONF_DIR=$PROJ_DIR/servers/spark/{{loc_dir_name}}
export SPARK_CONF_DIR=$SPARK_LOC_CONF_DIR/conf
export SPARK_LOG_DIR=$SPARK_LOC_CONF_DIR/log
export SPARK_PID_DIR=$SPARK_LOC_CONF_DIR/pid

# export CLASSPATH="$CLASSPATH:/gs/project/wst-164-ab/share/raglab_prod/software/derby/db-derby-10.10.2.0-bin/lib/derbyclient.jar"
# export SPARK_CLASSPATH=$CLASSPATH
# export SPARK_SUBMIT_CLASSPATH=$CLASSPATH

#export JVM_OPTS="$JVM_OPTS -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so"
#export SPARK_WORKER_DIR="/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work"

#export JAVA_LIBRARY_PATH=$JAVA_LIBRARY_PATH:/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-2.0.0-jars
#export SPARK_CLASSPATH=$SPARK_CLASSPATH:/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-1.6.1-jars/snappy-java-1.1.2.4.jar

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-2.0.0-jars
#export SPARK_LIBRARY_PATH=$SPARK_LIBRARY_PATH:/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/raglab_prod/software/spark/spark-2.0.0-jars

#add this to export HADOOP_OPTS="-Dorg.xerial.snappy.tempdir=/tmp -Dorg.xerial.snappy.lib.name=libsnappyjava.jnilib $HADOOP_OPTS"


# SPARK_MASTER_PORT / SPARK_MASTER_WEBUI_PORT, to use non-default ports
# SPARK_WORKER_CORES, to set the number of cores to use on this machine
# SPARK_WORKER_MEMORY, to set how much memory to use (for example 1000MB, 2GB)
# SPARK_WORKER_PORT / SPARK_WORKER_WEBUI_PORT
# SPARK_WORKER_INSTANCE, to set the number of worker processes per node
# SPARK_WORKER_DIR, to set the working directory of worker processes

#export HADOOP_OPTS="-Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so $HADOOP_OPTS"

#export SPARK_DAEMON_JAVA_OPTS="-Dspark.local.dir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so $SPARK_DAEMON_JAVA_OPTS"

#SPARK_JAVA_OPTS was detected (set to '-Dspark.local.dir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so -Dspark.local.dir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so -Dspark.local.dir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work -Dorg.xerial.snappy.lib.name=libsnappyjava.so ').
#This is deprecated in Spark 1.0+.

#Please instead use:
# - ./spark-submit with conf/spark-defaults.conf to set defaults for an application
# - ./spark-submit with --driver-java-options to set -X options for a driver
# - spark.executor.extraJavaOptions to set -X options for executors
# - SPARK_DAEMON_JAVA_OPTS to set java options for standalone daemons (master or worker)

# lzo compression
#SPARK_LIBRARY_PATH=$SPARK_LIBRARY_PATH:/path/to/your/hadoop-lzo/libs/native
#SPARK_CLASSPATH=$SPARK_CLASSPATH:/path/to/your/hadoop-lzo/java/libs

#this will work with warnings
#export SPARK_JAVA_OPTS="$SPARK_JAVA_OPTS -Dorg.xerial.snappy.tempdir=/nfs3_ib/ioannisr-mp2.nfs/tank/nfs/ioannisr/nobackup/share/expr/a000/work"

#http://stackoverflow.com/questions/23441142/class-com-hadoop-compression-lzo-lzocodec-not-found-for-spark-on-cdh-5
#https://github.com/twitter/hadoop-lzo
export SPARK_WORKER_CORES=4
export SPARK_WORKER_INSTANCES=1
export SPARK_WORKER_MEMORY=8g

#

