#!/usr/bin/env python
from __future__ import print_function

import subprocess
import optparse
import re
import os
import glob
import csv
import sys, random, itertools
import HTSeq
import shutil
from numpy.distutils.system_info import dfftw_threads_info

import pysam
import numpy as np
import time
import pandas as pd

import itertools

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib.backends.backend_pdf import PdfPages

import datetime

from pylab import *
import scipy.stats

import numpy as np
import pandas as pd


def rstyle(ax):
    """Styles an axes to appear like ggplot2
    Must be called after all plot and axis manipulation operations have been carried out (needs to know final tick spacing)
    """
    # set the style of the major and minor grid lines, filled blocks
    ax.grid(True, 'major', color='w', linestyle='-', linewidth=1.4)
    ax.grid(True, 'minor', color='0.92', linestyle='-', linewidth=0.7)
    ax.patch.set_facecolor('0.85')
    ax.set_axisbelow(True)

    #set minor tick spacing to 1/2 of the major ticks
    ax.xaxis.set_minor_locator(MultipleLocator( (plt.xticks()[0][1]-plt.xticks()[0][0]) / 2.0 ))
    ax.yaxis.set_minor_locator(MultipleLocator( (plt.yticks()[0][1]-plt.yticks()[0][0]) / 2.0 ))

    #remove axis border
    for child in ax.get_children():
        if isinstance(child, matplotlib.spines.Spine):
            child.set_alpha(0)

    # restyle the tick lines
    for line in ax.get_xticklines() + ax.get_yticklines():
        line.set_markersize(5)
        line.set_color("gray")
        line.set_markeredgewidth(1.4)

    # remove the minor tick lines
    for line in ax.xaxis.get_ticklines(minor=True) + ax.yaxis.get_ticklines(minor=True):
        line.set_markersize(0)

    #only show bottom left ticks, pointing out of axis
    rcParams['xtick.direction'] = 'out'
    rcParams['ytick.direction'] = 'out'
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')


    if ax.legend_ <> None:
        lg = ax.legend_
        lg.get_frame().set_linewidth(0)
        lg.get_frame().set_alpha(0.5)


def rhist(ax, data, **keywords):
    """Creates a histogram with default style parameters to look like ggplot2
    Is equivalent to calling ax.hist and accepts the same keyword parameters.
    If style parameters are explicitly defined, they will not be overwritten
    """

    defaults = {
                'facecolor' : '0.3',
                'edgecolor' : '0.28',
                'linewidth' : '1',
                'bins' : 100
                }

    for k, v in defaults.items():
        if k not in keywords: keywords[k] = v

    return ax.hist(data, **keywords)


def rbox(ax, data, **keywords):
    """Creates a ggplot2 style boxplot, is eqivalent to calling ax.boxplot with the following additions:

    Keyword arguments:
    colors -- array-like collection of colours for box fills
    names -- array-like collection of box names which are passed on as tick labels

    """

    hasColors = 'colors' in keywords
    if hasColors:
        colors = keywords['colors']
        keywords.pop('colors')

    if 'names' in keywords:
        ax.tickNames = plt.setp(ax, xticklabels=keywords['names'] )
        keywords.pop('names')

    bp = ax.boxplot(data, **keywords)
    pylab.setp(bp['boxes'], color='black')
    pylab.setp(bp['whiskers'], color='black', linestyle = 'solid')
    pylab.setp(bp['fliers'], color='black', alpha = 0.9, marker= 'o', markersize = 3)
    pylab.setp(bp['medians'], color='black')

    numBoxes = len(data)
    for i in range(numBoxes):
        box = bp['boxes'][i]
        boxX = []
        boxY = []
        for j in range(5):
          boxX.append(box.get_xdata()[j])
          boxY.append(box.get_ydata()[j])
        boxCoords = zip(boxX,boxY)

        if hasColors:
            boxPolygon = Polygon(boxCoords, facecolor = colors[i % len(colors)])
        else:
            boxPolygon = Polygon(boxCoords, facecolor = '0.95')

        ax.add_patch(boxPolygon)
    return bp

def roundup(num):
    # x = Math.log10(num).floor
    # num=(num/(10.0**x)).ceil*10**x

    x0 = np.array([num])

    x = np.floor(np.log10(x0))[0]
    y = (np.ceil(x0/(10.0**x))*10**x)[0]


    print("num, x, y : ", num, x, y )


    return y

def customized_box_plot(perc_df, axes, redraw = True, *args, **kwargs):
    """
    Generates a customized boxplot based on the given percentile values
    """
    n_box = perc_df.shape[0]
    print("n_box: ", n_box)

    box_plot = axes.boxplot([[-9, -4, 2, 4, 9],]*n_box, *args, **kwargs)
    # Creates len(percentiles) no of box plots


    # min_y, max_y = float('inf'), -float('inf')

    # all_vals = np.unique(perc_df[['q1', 'q2', 'q3', 'q4', 'q5']].values.ravel())

    # print("all_vals: \n", all_vals)

    # min_y = perc_df['q1'].min()
    # min_y = 0

    max_y = perc_df['q5'].max()

    # min_y = roundup(min_y)
    max_y = roundup(max_y)

    # The y axis is rescaled to fit the new box plot completely with 10%
    # of the maximum value at both ends
    # axes.set_ylim([min_y*1.1, max_y*1.1])
    axes.set_ylim([0, max_y])



    yticks = np.linspace(0, max_y, num=11, endpoint=True)
    axes.set_yticks(yticks, minor=False)


    # box_no=0
    for box_no in range(n_box):
        # ndex,value in enumerate(percentiles):
        #do stuff with array[n]


        print("box_no: ", box_no)
        q1_start = perc_df.at[box_no, 'q1']
        q2_start = perc_df.at[box_no, 'q2']
        q3_start = perc_df.at[box_no, 'q3']
        q4_start = perc_df.at[box_no, 'q4']
        q4_end =  perc_df.at[box_no, 'q5']
        fliers_xy = None
        print("q1_start: ", q1_start)



    # for box_no, (q1_start,
    #              q2_start,
    #              q3_start,
    #              q4_start,
    #              q4_end,
    #              fliers_xy) in enumerate(percentiles):

        # Lower cap
        box_plot['caps'][2*box_no].set_ydata([q1_start, q1_start])
        # xdata is determined by the width of the box plot

        # Lower whiskers
        box_plot['whiskers'][2*box_no].set_ydata([q1_start, q2_start])

        # Higher cap
        box_plot['caps'][2*box_no + 1].set_ydata([q4_end, q4_end])

        # Higher whiskers
        box_plot['whiskers'][2*box_no + 1].set_ydata([q4_start, q4_end])

        # Box
        box_plot['boxes'][box_no].set_ydata([q2_start,
                                             q2_start,
                                             q4_start,
                                             q4_start,
                                             q2_start])


        # Median
        box_plot['medians'][box_no].set_ydata([q3_start, q3_start])

        # Outliers
        # if fliers_xy is not None and len(fliers_xy[0]) != 0:
        #     If outliers exist
            # box_plot['fliers'][box_no].set(xdata = fliers_xy[0],
            #                                ydata = fliers_xy[1])
            #
            # min_y = min(q1_start, min_y, fliers_xy[1].min())
            # max_y = max(q4_end, max_y, fliers_xy[1].max())
        #
        # else:
        #     min_y = min(q1_start, min_y)
        #     max_y = max(q4_end, max_y)



        # box_no += 1

    # If redraw is set to true, the canvas is updated.
    # if redraw:
    #     ax.figure.canvas.draw()

    return box_plot






perc_df = pd.read_csv("depth_stats.csv",
                      sep=',', header=False,
                      index_col=[0],
                                     # usecols=[0, 1, 2, 3, 4, 5, 6],
                                     # names=["smp_idx", "smp_name", "grp",
                                     #        "sbgrp", "order_id", "graph_name",
                                     #        "other_name"])
                      )

print("perc_df: \n", perc_df)

# percentiles = [[-1.06, 0.39, 1.02, 1.66, 3.49,],
#                [-0.90, 0.36, 2.03, 1.68, 5.49,]]
#


pd.options.display.mpl_style = 'default'

with PdfPages("qc-bplot.pdf" ) as pdf:
    # fig, ax = plt.subplots()
    print("style.available: ", plt.style.available)

    plt.style.use(['ggplot'])

    fig = plt.figure(figsize=(10, 8))
    ax = plt.gca()




    bp = customized_box_plot(perc_df, ax, redraw=True, notch=0, sym='+', vert=1, whis=1.5)

    rstyle(ax)

    # colors = ['cyan', 'lightblue', 'lightgreen', 'tan', 'pink']
    # for patch, color in zip(b['boxes'], colors):
    #     patch.set_facecolor(color)
    setp(bp['boxes'], color='blue', linewidth=2.0)
    setp(bp['whiskers'], color='blue')
    setp(bp['fliers'], marker='None')
    setp(bp['caps'], color='blue', linewidth=1.0)
    setp(bp['medians'], color='red', linewidth=1.0)





    # plt.show()

    # xlabsmps = np.array(['aaa', 'bbb'], dtype="S50")
    xlabsmps = perc_df['graph_name'].values

    print("xlabsmps: \n", xlabsmps)

    # plt.xticks(np.arange(-1, xlabsmps.size +1, 1))
    ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')


    # plt.title('Duplication rates',  fontsize= 22)
    # ax.set_ylabel('Library size (M)', color='b')
    # for tl in ax.get_yticklabels():
    #     tl.set_color('b')


    ax.figure.canvas.draw()

    plt.tight_layout()

    pdf.savefig()  # saves the current figure into a pdf page
    plt.close()

"""
    plt.figure(figsize=(10, 8))
    # plt.set_dpi(100)
    # plt.plot(xvals, libsizes, 'b-')
    # plt.fill_between(xvals, percdups, libsizes)
    plt.plot(xvals, libsizes, 'b-')
    ax = plt.gca()
    ax.set_ylabel('Library size (M)', color='b')
    for tl in ax.get_yticklabels():
        tl.set_color('b')

    # ax1.set_xlabel('time (s)')
    # Make the y-axis label and tick labels match the line color.
    plt.xticks(np.arange(min(xvals), max(xvals)+1, 1))
    ax.set_xticklabels(xlabsmps, rotation=40, fontsize=10, ha='right')
    # plt.yticks(np.arange(min(libsizes), max(libsizes)+1, 20))


    # for tl in ax.get_xticklabels():
    #     tl.set_text()
    #     tl.set_color('g')
        # tl.set_rotation(45)
        # tl.set_fontsize(10)


    ax2 = plt.twinx()
    # s2 = np.sin(2*np.pi*t)
    ax2.plot(xvals, percdups, 'r-')
    # ax2.yticks(np.linspace(-1,1,5,endpoint=True))
    plt.yticks(np.arange(min(percdups), max(percdups)+5, 5))
    ax2.set_ylabel('Percent duplication', color='r')

    # start, end = ax2.get_ylim()
    # ax2.yaxis.set_ticks(np.arange(start, end, 0.712123))
    # ax2.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
    # ax2.set_ylabel('sin', color='r')
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
"""
