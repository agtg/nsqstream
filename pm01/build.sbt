
name := """movie-store"""
organization := "ca.agtg"

version := "0.4-1"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

//scalaVersion := "2.12.16"
scalaVersion := "2.13.8"

libraryDependencies += guice

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,

  // Enable reactive mongo for Play 2.8
  "org.reactivemongo" %% "play2-reactivemongo" % "0.20.13-play28",

  // Provide JSON serialization for reactive mongo
  "org.reactivemongo" %% "reactivemongo-play-json-compat" % "1.0.1-play28",

  // Provide BSON serialization for reactive mongo
  "org.reactivemongo" %% "reactivemongo-bson-compat" % "0.20.13",

  // Provide JSON serialization for Joda-Time
  "com.typesafe.play" %% "play-json-joda" % "2.7.4",
)

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.10"





//libraryDependencies += "com.typesafe" % "config" % "1.4.1"

//libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.12"
//libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % "test"
//libraryDependencies ++= Seq("org.reactivemongo" %% "reactivemongo" % "1.1.0-RC4")
//libraryDependencies ++= Seq("org.reactivemongo" %% "reactivemongo" % "1.0")



enablePlugins(PackPlugin)

// [Optional] Specify main classes manually
// This example creates `hello` command (target/pack/bin/hello) that calls org.mydomain.Hello#main(Array[String])
packMain := Map("play" -> "play.core.server.ProdServerStart")

