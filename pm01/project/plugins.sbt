
//addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.7")
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.16")

//addSbtPlugin("org.foundweekends.giter8" % "sbt-giter8-scaffold" % "0.11.0")
addSbtPlugin("org.foundweekends.giter8" % "sbt-giter8-scaffold" % "0.13.1")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.14")
