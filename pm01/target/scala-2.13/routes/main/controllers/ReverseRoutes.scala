// @GENERATOR:play-routes-compiler
// @SOURCE:conf/routes

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers {

  // @LINE:7
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:7
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:9
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:13
  class ReverseMovieController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def create(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "movies")
    }
  
    // @LINE:14
    def findOne(id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "movies/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
    // @LINE:13
    def findAll(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "movies")
    }
  
    // @LINE:17
    def delete(id:String): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "movies/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
    // @LINE:16
    def update(id:String): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "movies/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("id", id)))
    }
  
  }

  // @LINE:25
  class ReverseJobController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:25
    def freejobs(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "jobs/freejobs")
    }
  
    // @LINE:26
    def submit_tsk(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "jobs/sendtsk")
    }
  
    // @LINE:28
    def tsks_status(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "jobs/tskstatus")
    }
  
  }


}
