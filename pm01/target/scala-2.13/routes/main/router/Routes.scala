// @GENERATOR:play-routes-compiler
// @SOURCE:conf/routes

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  HomeController_0: controllers.HomeController,
  // @LINE:9
  Assets_1: controllers.Assets,
  // @LINE:13
  MovieController_2: controllers.MovieController,
  // @LINE:25
  JobController_3: controllers.JobController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    HomeController_0: controllers.HomeController,
    // @LINE:9
    Assets_1: controllers.Assets,
    // @LINE:13
    MovieController_2: controllers.MovieController,
    // @LINE:25
    JobController_3: controllers.JobController
  ) = this(errorHandler, HomeController_0, Assets_1, MovieController_2, JobController_3, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_0, Assets_1, MovieController_2, JobController_3, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """movies""", """controllers.MovieController.findAll()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """movies/""" + "$" + """id<[^/]+>""", """controllers.MovieController.findOne(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """movies""", """controllers.MovieController.create()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """movies/""" + "$" + """id<[^/]+>""", """controllers.MovieController.update(id:String)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """movies/""" + "$" + """id<[^/]+>""", """controllers.MovieController.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """jobs/freejobs""", """controllers.JobController.freejobs()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """jobs/sendtsk""", """controllers.JobController.submit_tsk()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """jobs/tskstatus""", """controllers.JobController.tsks_status()"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_0.index(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_MovieController_findAll2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("movies")))
  )
  private[this] lazy val controllers_MovieController_findAll2_invoker = createInvoker(
    MovieController_2.findAll(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MovieController",
      "findAll",
      Nil,
      "GET",
      this.prefix + """movies""",
      """ Routes""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_MovieController_findOne3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("movies/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MovieController_findOne3_invoker = createInvoker(
    MovieController_2.findOne(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MovieController",
      "findOne",
      Seq(classOf[String]),
      "GET",
      this.prefix + """movies/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_MovieController_create4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("movies")))
  )
  private[this] lazy val controllers_MovieController_create4_invoker = createInvoker(
    MovieController_2.create(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MovieController",
      "create",
      Nil,
      "POST",
      this.prefix + """movies""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_MovieController_update5_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("movies/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MovieController_update5_invoker = createInvoker(
    MovieController_2.update(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MovieController",
      "update",
      Seq(classOf[String]),
      "PUT",
      this.prefix + """movies/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_MovieController_delete6_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("movies/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_MovieController_delete6_invoker = createInvoker(
    MovieController_2.delete(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.MovieController",
      "delete",
      Seq(classOf[String]),
      "DELETE",
      this.prefix + """movies/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_JobController_freejobs7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("jobs/freejobs")))
  )
  private[this] lazy val controllers_JobController_freejobs7_invoker = createInvoker(
    JobController_3.freejobs(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.JobController",
      "freejobs",
      Nil,
      "GET",
      this.prefix + """jobs/freejobs""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_JobController_submit_tsk8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("jobs/sendtsk")))
  )
  private[this] lazy val controllers_JobController_submit_tsk8_invoker = createInvoker(
    JobController_3.submit_tsk(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.JobController",
      "submit_tsk",
      Nil,
      "POST",
      this.prefix + """jobs/sendtsk""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_JobController_tsks_status9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("jobs/tskstatus")))
  )
  private[this] lazy val controllers_JobController_tsks_status9_invoker = createInvoker(
    JobController_3.tsks_status(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.JobController",
      "tsks_status",
      Nil,
      "GET",
      this.prefix + """jobs/tskstatus""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_0.index())
      }
  
    // @LINE:9
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_1.versioned(path, file))
      }
  
    // @LINE:13
    case controllers_MovieController_findAll2_route(params@_) =>
      call { 
        controllers_MovieController_findAll2_invoker.call(MovieController_2.findAll())
      }
  
    // @LINE:14
    case controllers_MovieController_findOne3_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_MovieController_findOne3_invoker.call(MovieController_2.findOne(id))
      }
  
    // @LINE:15
    case controllers_MovieController_create4_route(params@_) =>
      call { 
        controllers_MovieController_create4_invoker.call(MovieController_2.create())
      }
  
    // @LINE:16
    case controllers_MovieController_update5_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_MovieController_update5_invoker.call(MovieController_2.update(id))
      }
  
    // @LINE:17
    case controllers_MovieController_delete6_route(params@_) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_MovieController_delete6_invoker.call(MovieController_2.delete(id))
      }
  
    // @LINE:25
    case controllers_JobController_freejobs7_route(params@_) =>
      call { 
        controllers_JobController_freejobs7_invoker.call(JobController_3.freejobs())
      }
  
    // @LINE:26
    case controllers_JobController_submit_tsk8_route(params@_) =>
      call { 
        controllers_JobController_submit_tsk8_invoker.call(JobController_3.submit_tsk())
      }
  
    // @LINE:28
    case controllers_JobController_tsks_status9_route(params@_) =>
      call { 
        controllers_JobController_tsks_status9_invoker.call(JobController_3.tsks_status())
      }
  }
}
