package repositories

import ca.agtg.naseq._
import play.api.Logger

import javax.inject.{Inject, Singleton}
import scala.collection.mutable.{ArrayBuffer, Map}
import scala.concurrent.{ExecutionContext, Future}


//case class TaskItem(tsk_idx:Int, method: String, params: String)
case class TaskItem(tsk_idx:String, method: String, params: String)

@Singleton
class SrvRepository @Inject()(
   implicit executionContext: ExecutionContext
  ){

  val applog=Logger("application")
  val apcnf: AppConfig = AppConfig()

  val jobs_ab:ArrayBuffer[(TaskItem,Worker,Future[Double])] = ArrayBuffer()
  val max_jobs=4

//  val jobs_ab:ArrayBuffer[TaskItem] = ArrayBuffer()

//  https://stackoverflow.com/questions/27285760/play-json-formatter-for-mapint
//  val tasks_status_mp: Map[Int, String] = Map[Int, String]()
//  this one is standard with Json.toJson
    val tasks_status_mp: Map[String, String] = Map[String, String]()



}

