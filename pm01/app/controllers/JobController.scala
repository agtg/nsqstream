package controllers

import ca.agtg.naseq._
import play.api.Logger
//import play.Logger
//import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import repositories._

import javax.inject._
import scala.concurrent.ExecutionContext

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class JobController @Inject()(implicit executionContext: ExecutionContext,
                              val srvRepository: SrvRepository,
                              val controllerComponents: ControllerComponents) extends BaseController {

  val playlog: Logger = Logger("play")
  playlog.info("JOB CONTROLLER CONSTRUCTOR")
  val applog=Logger("application")
  applog.info("finally info")

  //  val srv_rep= new SrvRepository()
  //  var srv_svc = new SrvService(srv_rep)
  var srv_svc = new SrvService(srvRepository)
  //srv.run(args)

//  Logger.info("after run..")


  def run() = Action { implicit request: Request[AnyContent] =>

//    Logger.info("A log message")
//    Logger.error("Oops")

//    Logger.info(s"Free jobs ${srv_svc.free_jobs}")

    Ok("This is the main functions\n")
  }

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def freejobs() = Action { implicit request: Request[AnyContent] =>

    val resp1= Map("free_jobs" -> srv_svc.free_jobs)
    val resp2=Json.toJson(resp1)
    Ok(resp2)
//    Ok(s">${srv_svc.free_jobs.toString}<\n")
  }


  //  curl -v -d '{"method": "trim_spln";"params"="{\"spln_idx\": 2}"}' -H 'Content-Type: application/json' -X POST localhost:9000/jobs/submitjob
  //need to run it using R
  def submit_tsk() = Action { implicit request: Request[AnyContent] =>
    val content = request.body
    val jsonObject = content.asJson
//    Logger.info(s"jsonObject: $jsonObject")
    val json = jsonObject.get
//    Logger.info(s"received:")
//    Logger.info(json.toString())

    val tsk_idx = (json \ "tsk_idx").as[String]
    srvRepository.tasks_status_mp(tsk_idx) = "submitted"
    val params1: JsValue = (json \ "params").get
    val params = Json.stringify(params1)
    println(params1)
    println(params)

    //    val tsk= TaskItem(tsk_idx=tsk_idx, method=(json \ "method").as[String], params=(json \ "params").as[String])
    val tsk = TaskItem(tsk_idx = tsk_idx, method = (json \ "method").as[String], params = params)
//    Logger.info(s"tsk: $tsk")
    srv_svc.submit_task(tsk)

    Ok(s">\n")

  }
  def tsks_status() = Action { implicit request: Request[AnyContent] =>

    applog.info("info in tsks_status()...")
    println("println in tsks_status()...")

    val resp=Json.toJson(srvRepository.tasks_status_mp)
    Ok(resp)

  }





}
