import com.google.inject.AbstractModule

class EagerLoaderModule extends AbstractModule {
  override def configure() = {
    bind(classOf[ApplicationStart]).asEagerSingleton()
  }
}

