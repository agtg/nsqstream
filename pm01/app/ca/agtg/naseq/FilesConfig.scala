package ca.agtg.naseq

import scala.collection.immutable
import scala.sys.process._

class FilesConfig(val proj_path: String) {

  //rest of the path
  //constructor
  val cwd = "pwd".!!  //*nix OS
  val dat_path: String = scala.util.Properties.envOrElse("NASEQ_PROJ_DAT", cwd)
  val glo_path: String = scala.util.Properties.envOrElse("NASEQ_PROJ_GLO", cwd)
  val scr_path: String = scala.util.Properties.envOrElse("NASEQ_PROJ_SCR", cwd)
  val gnm_path: String = scala.util.Properties.envOrElse("NASEQ_PROJ_GNM", cwd)

  val trimmomatic_jar=scala.util.Properties.envOrElse("TRIMMOMATIC_JAR", "")

  //var spln_row:immutable.Map[String,String]= immutable.Map()
  var spln_row:immutable.Map[String,String]=immutable.Map()

  def trim_illuminaclip: String = {
    var res=
      spln_row("trim") match {
        case "rna1" => {
          val adapt=s"${gnm_path}/supl/adapters-nextera.fa"
          val illuminaclip = s"ILLUMINACLIP:${adapt}:4:30:30:8:true HEADCROP:6 TRAILING:30 MINLEN:30"
          illuminaclip
        }

      }
    res

  }
  def al5_dir: String = {
    s"${scr_path}/align5"
  }

  def spln_fq1: String={
    s"${dat_path}/${spln_row("fastq1")}"
  }

  def spln_fq2: String={
    s"${dat_path}/${spln_row("fastq2")}"
  }
  def spln_rp1: String={
    s"${al5_dir}/rp1_${spln_row("spln_idx")}.fq.gz"
  }
  def spln_rs1: String={
    s"${al5_dir}/rs1_${spln_row("spln_idx")}.fq.gz"
  }
  def spln_rp2: String={
    s"${al5_dir}/rp2_${spln_row("spln_idx")}.fq.gz"
  }
  def spln_rs2: String={
    s"${al5_dir}/rs2_${spln_row("spln_idx")}.fq.gz"
  }
  def trim_out: String={
    s"${al5_dir}/trim_${spln_row("spln_idx")}.out"
  }
  def aln_bam: String={
    s"${al5_dir}/aln_${spln_row("spln_idx")}.bam"
  }
  def align1_out: String={
    s"${al5_dir}/align1_${spln_row("spln_idx")}.out"
  }
  def align2_out: String={
    s"${al5_dir}/align2_${spln_row("spln_idx")}.out"
  }
  def aln_nsrt_bam: String={
    s"${al5_dir}/aln_nsrt_${spln_row("spln_idx")}.bam"
  }
  def aln_csrt_bam: String={
    s"${al5_dir}/aln_csrt_${spln_row("spln_idx")}.bam"
  }
  def trancriptome_gtf: String={
//    s"/home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38/all_transcr/h4Hg38_geM36/bt2_ix/h4Hg38_geM36.gtf"
    s"/home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38_dist/h4Hg38_geM36.gtf"
  }
  def genome_chromsizes: String={
    //    s"/home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38/all_transcr/h4Hg38_geM36/bt2_ix/h4Hg38_geM36.gtf"
    s"/home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38_dist/h4Hg38.chrominfo.txt"
  }

  def aln_feat_csv: String={
    s"${al5_dir}/aln_feat_${spln_row("spln_idx")}.csv"
  }
  def aln_count_csv: String={
    s"${al5_dir}/aln_count_${spln_row("spln_idx")}.csv"
  }
  def count1_out: String={
    s"${al5_dir}/count1_${spln_row("spln_idx")}.out"
  }
  def stringtie1_out: String={
    s"${al5_dir}/stringtie1_${spln_row("spln_idx")}.out"
  }
  def stringtie1_gtf: String={
    s"${al5_dir}/stringtie1_${spln_row("spln_idx")}.out.gtf"
  }
  def stringtie1_genePred: String={
    s"${al5_dir}/stringtie1_${spln_row("spln_idx")}.genePred"
  }
  def stringtie1_bigGenePredEx4_txt: String={
    s"${al5_dir}/stringtie1_${spln_row("spln_idx")}.bigGenePredEx4.txt"
  }
  def stringtie1_bigGenePredEx4_sorted_txt: String={
    s"${al5_dir}/stringtie1_${spln_row("spln_idx")}.bigGenePredEx4.sorted.txt"
  }

  def stringtie_tag: String={
    s"STRG_${spln_row("spln_idx")}"
  }

  def stringtie1_gtf2bb_out: String={
    s"${al5_dir}/stringtie_gtf2bb_${spln_row("spln_idx")}.out"
  }
  def stringtie_bb: String={
    s"${al5_dir}/stringtie_${spln_row("spln_idx")}.bb"
  }
  def cov_ori_bg: String={
    s"${al5_dir}/cov_ori_${spln_row("spln_idx")}.bedgraph"
  }
  def cov_srt_bg: String={
    s"${al5_dir}/cov_srt_${spln_row("spln_idx")}.bedgraph"
  }
  def cov_bw: String={
    s"${al5_dir}/cov_${spln_row("spln_idx")}.bw"
  }
  def wiggle_out: String={
    s"${al5_dir}/wiggle_${spln_row("spln_idx")}.out"
  }

  def tsk_lck_d: String={
    s"${al5_dir}/tsk_lck"
  }



  override def toString(): String = {
    s"""
       |Proj Path: $proj_path
       |Crust Type:
       |Toppings:
        """.stripMargin
  }



}

