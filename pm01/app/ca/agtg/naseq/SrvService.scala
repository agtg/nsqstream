package ca.agtg.naseq

import com.typesafe.config.ConfigFactory
import play.Logger
import play.api.Logging
import reactivemongo.api.DB

import java.io.File
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.sys.process._
// Importing break package
import com.github.tototoshi.csv._
import play.api.libs.json._
import reactivemongo.api.{AsyncDriver, MongoConnection}
import repositories._

import scala.concurrent.duration.Duration
import scala.util.control.Breaks._



trait Sleeppy {
  def sleep(time: Long): Unit = Thread.sleep(time)
}

case class AppConfig() extends Logging {
  //constructor
  val cwd = "pwd".!!  //*nix OS
  val proj_path: String = scala.util.Properties.envOrElse("NASEQ_PROJ_DIR", cwd)
  val app_conf_f=s"$proj_path/conf_naseq/naseq.conf"
  val config = ConfigFactory.parseFile( new java.io.File(app_conf_f))
//  val sparkConfig = config.getConfig("com.ram.batch")
  val run_d= config.getString("run_d")
  val run_d2 = s"$proj_path/$run_d"
  Logger.info(s"run_d2: $run_d2")

  Logger.info("Hello world in JobController constructor!")

  val splns_f=s"$proj_path/conf_naseq/splns.csv"
  var reader = CSVReader.open(new File(splns_f))
  val splns: List[Map[String,String]] =reader.allWithHeaders()
  reader.close()
  Logger.info(s"splns: $splns(0)")

  val proj_srv_f=s"$proj_path/conf/proj_srv.csv"
  reader = CSVReader.open(new File(proj_srv_f))
  val proj_srv_content: List[List[String]] =reader.all()
  val proj_srv=proj_srv_content(0)(0)
  reader.close()
  Logger.info(s"proj_srv: ${proj_srv}")


  // Create Config from File
  //val config = ConfigFactory.load(app_conf_f).getConfig("com.ram.batch")



  // connect to the replica set composed of `host1:27018`, `host2:27019` and `host3:27020`
  // and authenticate on the database `somedb` with user `user123` and password `passwd123`
  val uri = s"mongodb://usertest:usertest@${proj_srv}/test"
  val mgdriver = AsyncDriver()

  def connection7(driver: AsyncDriver): Future[MongoConnection] = for {
    parsedUri <- MongoConnection.fromString(uri)
    conF <- driver.connect(parsedUri)
  } yield conF

  val conF=connection7(mgdriver)
  val con:MongoConnection  = Await.result(conF, Duration.Inf)
  println("connection: ",con)

  def db1F: Future[DB] = conF.flatMap(_.database("test"))


  def personCollection = db1F.map(_.collection("person"))



}



// Custom persistent types
case class AllSmps(val spln_idx: Int, val skip: Int, val smp_name: String, val run: Int, val lane: Int,
                   val lib: String, val fastq1: String, val fastq2: String)
// or provide a custom one


import javax.inject._
class SrvService (srv_rep: SrvRepository) extends Sleeppy {

  def free_jobs:Int = {
    var wkjbs=0
    for (jidx <- 0 until srv_rep.jobs_ab.size) {
      var tsk = srv_rep.jobs_ab(jidx)
      var completed = tsk._3.isCompleted
      //println(s"jidx: ${jidx}, completed: ${completed}")
      srv_rep.applog.info(s"jidx: ${jidx}, completed: ${completed}")

      if (!completed) {
        wkjbs += 1
      }

    }
    println(s"wkjbs: ${wkjbs}, ")
    (srv_rep.max_jobs-wkjbs)
  }

  def submit_task(tsk:TaskItem):Unit = {
    val wkr= new Worker(srv_rep,tsk)
    srv_rep.tasks_status_mp(tsk.tsk_idx)="running"

   //    var wkrF:Future[Double] = null
   //    var tup:(Task,Worker,Future[Double]) = null

   var free_idx = -1

   breakable {
     for (jidx <- 0 until srv_rep.jobs_ab.size) {
       var tsk = srv_rep.jobs_ab(jidx)
       var completed= tsk._3.isCompleted
       println(s"Value of jidx: $jidx, $completed")
       if (completed) {
         free_idx=jidx
         break()
       }
     }
   }
   println(s"free_idx: $free_idx")
   if (free_idx>=0) {
     //var tup3=jobs_ab(free_idx)
     srv_rep.jobs_ab(free_idx)=((tsk,wkr,wkr.runF()))

   } else if ( (free_idx == -1) && (srv_rep.jobs_ab.size <= srv_rep.max_jobs)) {
     srv_rep.jobs_ab += ((tsk,wkr,wkr.runF()))
   } else {
     println("should not happen, already sure we can submit")
     sleep(2000)
   }
//    val wkr=new Worker(apcnf,atsk)
//    println()



  }


  def run(args: Array[String]): Unit = {

    println(s"Hello world of pains! ${srv_rep.apcnf.proj_path}")
    println(s"Hello world of pains! ${srv_rep.apcnf.app_conf_f}")


    println(s"Hello world of pains3 ! ${srv_rep.apcnf.run_d2}")
    //

    for (idx <-0 until 20) {
       var idx2=idx.toString


//    new task here
     val tsk=new TaskItem(tsk_idx=idx2, method="align_smp", params=s"{smp_idx: $idx}")
       println(s"new job: $free_jobs")
      while (free_jobs==0) {
        println("Wait for a job to become available")
        sleep(2000)
      }
      submit_task(tsk)

    } //end generating jobs
    println("finished submitting jobs !")

    //wkrF=wkr.runF()
    //tup = (tsk,wkr,wkrF)



//    wkr.run()


    //val compl=wkr.runF().isCompleted


//    var ab:ArrayBuffer[(Task,Worker,Future[Double])] = ArrayBuffer()
//    ab+=t





/**
    wkrF.onComplete {
      case Success(x) => {
        //val totalTime = deltaTime(startTime)
        //println(s"In Success case, time delta: ${totalTime}")
        println(s"Result from wkrF is: $x")
      }
      case Failure(e) => e.printStackTrace
    }
**/
    sleep(50000)

  }

}
