package ca.agtg.naseq

import play.api.libs.json.Json
import reactivemongo.api.Cursor
import reactivemongo.api.bson.{BSONDocumentReader, Macros, document}
import repositories.{SrvRepository, TaskItem}

import java.io.File
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Set
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import scala.sys.process._
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path


class Worker(var srv_rep:SrvRepository, val tsk:TaskItem) extends Sleeppy {

  val params=Json.parse(tsk.params)

  var flcnf =  new FilesConfig(srv_rep.apcnf.proj_path)

  def allsmpsCollection = srv_rep.apcnf.db1F.map(_.collection("allsmps"))
  implicit def allsmpsReader: BSONDocumentReader[AllSmps] = Macros.reader[AllSmps]

  def findAllSmpsByspln_idx(spln_idx: Int): Future[List[AllSmps]] = {
    allsmpsCollection.flatMap(_.find(document("spln_idx" -> spln_idx)). // query builder
      cursor[AllSmps](). // using the result cursor
      collect[List](-1, Cursor.FailOnError[List[AllSmps]]()))
  }
  // ... deserializes the document using allsmpsReader



  def setup_files():Unit = {

    // Using MongoDB from Application conf
    var splnF=findAllSmpsByspln_idx(2)
    var spln=Await.result(splnF,Duration.Inf)
    println(spln(0))


    tsk.method match {
      case "align_smp" | "align2_smp" => {println("setup smp")}
      case "spln_trim" | "spln_align" | "spln_count" | "spln_stringtie" | "spln_gtf2bb" | "spln_wiggle" => {
       println("setup spln")
        val spln_idx:Int =(params \ "spln_idx").as[Int]
        flcnf.spln_row=srv_rep.apcnf.splns(spln_idx)
//        println(s"spln_row: ${flcnf.spln_row}")
//        println(s"spln_fq1: ${flcnf.spln_fq1}")
//        println(s"spln_fq2: ${flcnf.spln_fq2}")
//        println(s"trim_illuminaclip: ${flcnf.trim_illuminaclip}")

      }
      case _  => { println("use some other params")
       println((params \ "spln_idx").as[Int] )

      }
    }





  }

  /**
   *  //  we do not call it directly anymore
   *  def run(): Unit = {
   *  //simulate some random wait here
   *  println(flcnf)
   *  println(s"tsk: ${tsk.method} | ${tsk.params}")
   *  }
   * */

  def run_method: ArrayBuffer[Int] = {
    val res: ArrayBuffer[Int] = ArrayBuffer()

    tsk.method match {
      case "align_smp" | "align2_smp" => {
        println("setup smp")
      }
      case "spln_trim" => {
        println("run spln_trim")
        res += spln_trim
      }
      case "spln_align" => {
        println("run spln_align")
        res += spln_align
      }
      case "spln_count" => {
        println("run spln_count")
        res += spln_count
      }
      case "spln_stringtie" => {
        println("run spln_stringtie")
        res += spln_stringtie
      }
      case "spln_gtf2bb" => {
        println("run spln_gtf2bb")
        res += spln_gtf2bb
      }
      case "spln_wiggle" => {
        println("run spln_wiggle")
        res += spln_wiggle
      }
      case _ => {
        println("use some other params")
        println((params \ "spln_idx").as[Int])

      }
    }

    res
  }

  def spln_trim:Int = {

    val cmd0 = s"java -jar ${flcnf.trimmomatic_jar} PE -threads ${2} -phred33 " +
      s"${flcnf.spln_fq1} ${flcnf.spln_fq2} ${flcnf.spln_rp1} ${flcnf.spln_rs1} ${flcnf.spln_rp2} ${flcnf.spln_rs2} " +
      s" ${flcnf.trim_illuminaclip} &> ${flcnf.trim_out} "
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(res)

    res
  }

  def spln_align:Int = {

    val cmd0 = raw"" +
      raw"$$BSFTW/bioinfo/hisat2-2.2.1/hisat2 " +
      raw" --rg-id ${flcnf.spln_row("spln_idx")} --rg LB:rglb --rg PL:ILLUMINA --rg PU:rgpu --rg SM:${flcnf.spln_row("spln_idx")} " +
      raw" -x /home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38/all_transcr/h4Hg38_geM36/hisat2_ix/h4Hg38_geM36/h4Hg38_geM36 " +
      raw" -p 1  -1 ${flcnf.spln_rp1} -2 ${flcnf.spln_rp2} 2>${flcnf.align1_out}" +
      raw" | $$BSFTW/bioinfo/sambamba-inst/bin/sambamba view /dev/stdin --sam-input -f bam -o ${flcnf.aln_bam} &> ${flcnf.align2_out} "
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(s"res: ${res}")

    res
  }

  def spln_count:Int = {

    val cmd0 = raw"" +
      raw"$$BSFTW/bioinfo/sambamba-inst/bin/sambamba sort -t 1 -n -o ${flcnf.aln_nsrt_bam} ${flcnf.aln_bam} 2>${flcnf.count1_out} && " +
      raw"$$BSFTW/bioinfo/subread-2.0.3-Linux-x86_64/bin/featureCounts -T 1 -a ${flcnf.trancriptome_gtf} -F GTF -p -g gene_id -o ${flcnf.aln_feat_csv}  ${flcnf.aln_nsrt_bam} 2>>${flcnf.count1_out} && " +
      raw"""cat ${flcnf.aln_feat_csv} | grep -v '^#' | awk '{ print $$1 "\t" $$7 }' > ${flcnf.aln_count_csv} 2>>${flcnf.count1_out}"""
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(res)

    res
  }

//  cat merged.annotated.gtf | awk '$1=="chr10" {print $0}' | awk '$3=="transcript" {print $0}'  | awk '$4 <100360000 && $5 > 100280000 { print $0}'
  def spln_stringtie:Int = {

    val cmd0 = raw"" +
      raw"$$BSFTW/bioinfo/sambamba-inst/bin/sambamba sort -t 2 -o ${flcnf.aln_csrt_bam} ${flcnf.aln_bam} 2>${flcnf.stringtie1_out} && " +
      raw"$$BSFTW/bioinfo/stringtie-2.2.1.Linux_x86_64/stringtie -p 2 -o ${flcnf.stringtie1_gtf}  ${flcnf.aln_csrt_bam} -l ${flcnf.stringtie_tag} 2>>${flcnf.stringtie1_out} "
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(res)

    res
  }

  def spln_gtf2bb:Int = {

    val cmd0 = raw"" +
      raw"$$BSFTW/bioinfo/ucsc/bin/gtfToGenePred -genePredExt ${flcnf.stringtie1_gtf} ${flcnf.stringtie1_genePred}  2>${flcnf.stringtie1_gtf2bb_out} && " +
      raw"$$BSFTW/bioinfo/ucsc/bin/genePredToBigGenePred  ${flcnf.stringtie1_genePred} ${flcnf.stringtie1_bigGenePredEx4_txt} 2>>${flcnf.stringtie1_gtf2bb_out} && " +
      raw"$$BSFTW/bioinfo/ucsc/bin/bedSort  ${flcnf.stringtie1_bigGenePredEx4_txt} ${flcnf.stringtie1_bigGenePredEx4_sorted_txt} 2>>${flcnf.stringtie1_gtf2bb_out} && " +
      raw"$$BSFTW/bioinfo/ucsc/bin/bedToBigBed -type=bed12+8 -tab -as=$$BSFTW/bioinfo/ucsc/bin/bigGenePred.as ${flcnf.stringtie1_bigGenePredEx4_sorted_txt} ${flcnf.genome_chromsizes} ${flcnf.stringtie_bb}"
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(res)

    res
  }

  def spln_wiggle:Int = {

    val cmd0 = raw"" +
      raw"$$BSFTW/bioinfo/bedtools/bedtools genomecov -ibam ${flcnf.aln_csrt_bam} -bg > ${flcnf.cov_ori_bg} 2>${flcnf.wiggle_out}  && " +
      raw"$$BSFTW/bioinfo/ucsc/bin/bedSort ${flcnf.cov_ori_bg} ${flcnf.cov_srt_bg} 2>>${flcnf.wiggle_out} && " +
      raw"$$BSFTW/bioinfo/ucsc/bin/bedGraphToBigWig ${flcnf.cov_srt_bg} ${flcnf.genome_chromsizes} ${flcnf.cov_bw} 2>>${flcnf.wiggle_out}"
    println(s"cmd0: ${cmd0}")
    val p0=Process(Seq("/bin/sh","-c",cmd0),new File(s"${flcnf.al5_dir}"))
    val res=p0.!

    //p0.run().exitValue()

    println(res)

    res
  }


  // Here is the work done
  def runF(): Future[Double] = Future {
    var func_res=0.0
    //val r = scala.util.Random
//    val randomSleepTime = r.nextInt(3000)
//    println(s"For, sleep time is $randomSleepTime")
//    val randomPrice = r.nextDouble() * 1000
    //this is the working
//    sleep(randomSleepTime)
    println(flcnf)
    //println(s"running tsk content: ${tsk.method} | ${tsk.params}")
    srv_rep.applog.info(s"running tsk content: ${tsk.method} | ${tsk.params}")
//    here we  add , but we need to add it before, and now update status based on function exitcode.
//    srv_rep.tsk_ab += tsk
    val tsk_lck_fname=s"${flcnf.tsk_lck_d}/${tsk.tsk_idx}.lck"
    val tsk_lck_exists=Files.exists(Paths.get(tsk_lck_fname))
    if (!tsk_lck_exists) {
      val fp: Path = Paths.get(tsk_lck_fname)
      Files.createDirectories(fp.getParent)
      Files.createFile(fp)
      val res=run_method
      func_res=res.reduceLeft(_ max _).toDouble
      println(s"runF() res: ${tsk.method} | ${tsk.params} | ${res}")
      //update status if no error done, if error, failed
      Files.delete(fp)
      srv_rep.tasks_status_mp(tsk.tsk_idx)= func_res match {
        case 0.0  => "done"
        case _  => "failed"  // the default, catch-all
      }
      //      srv_rep.tasks_status_mp(tsk.tsk_idx)="done"
//      func_res=0.0
    } else {
      //fail task run
      srv_rep.tasks_status_mp(tsk.tsk_idx)="taken"
      func_res=1.0
    }

//    randomPrice
    func_res

  }

  //part of the constructor
  //given a task method, use the parameters to setup files
 setup_files()


}
