#!/bin/sh

$BSFTW/bioinfo/sambamba-inst/bin/sambamba sort -n -o aln_1.srt.bam aln_1.bam 

$BSFTW/bioinfo/subread-2.0.3-Linux-x86_64/bin/featureCounts -T 6 -a h4Hg38_geM36.gtf -F GTF -p -g gene_id -o aln_1_feat.csv aln_1.srt.bam

cat aln_1_feat.csv | grep -v '^#' | awk '{ print $1 "\t" $7 }' > aln1_counts.csv





