#!/bin/bash

cd /scratch/badescud/proj/pdxdata/work
rm -fr run_/
rm -fr __*

cellranger count --id=run_pdx1735 \
--fastqs=/scratch/badescud/proj/pdxdata/fq3 \
--sample=PDX1735 \
--transcriptome=/scratch/badescud/proj/pdxdata/ref/h4Hg38 \
--localcores 14 \
--localmem 40

