#!/bin/sh

FILE=/etc/resolv.conf

TSK="tsk1.lck"
#touch $TSK


if [ -f "$TSK" ]; then
    echo "$TSK exists."
    exit 200
else 
    echo "$TSK does not exist."
    touch $TSK
    sleep 5
    rm $TSK
fi


#exec 200<$TSK
#flock -n 200 || exit 1



