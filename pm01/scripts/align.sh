#!/bin/sh

$BSFTW/bioinfo/hisat2-2.2.1/hisat2 \
--rg-id 0 --rg LB:rglb --rg PL:ILLUMINA --rg PU:rgpu --rg SM:0 \
-x /home/badescud/projects/def-ioannisr/badescud/igenomes/h4Hg38/all_transcr/h4Hg38_geM36/hisat2_ix/h4Hg38_geM36/h4Hg38_geM36 \
-p 2  -1 rp1_0.fq.gz -2 rp2_0.fq.gz \
| $BSFTW/bioinfo/sambamba-inst/bin/sambamba view /dev/stdin --sam-input -f bam -o aln_1.bam




