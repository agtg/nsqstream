
# mount whole genap structure by sshfs on local folder
# use one of the authorized keys, in /ssh on genap (xclip and paste using filebrowser)

sshfs -p 22004 sftp_cy4@sftp-arbutus.genap.ca:/ genap_sftp/ -o IdentityFile=~/.ssh/ids/id_rsa91

# deploy
#rsync -rvP --del --partial run_pdx1735/outs/ genap_sftp/datahub698/pdx1735/  --exclude *.bam --exclude *.bai --exclude *.h5 --exclude *.cloupe
rsync -rvP --del --partial run_GCRC1915Control/outs/ genap_sftp/datahub698/pdx1735/  --exclude *.bam --exclude *.bai --exclude *.h5 --exclude *.cloupe


# update deployed files to make them visible
chmod -R 755 genap_sftp/datahub698/

