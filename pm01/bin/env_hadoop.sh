#!/bin/bash

#cp $DERBY_HOME/lib/derbyclient.jar $SPARK_HOME/jars/

export JAVA_HOME=/home/dbadescu/share/raglab_prod/hadoop/graalvm-ce-java11-22.1.0
export PATH=$JAVA_HOME/bin:$PATH


export DERBY_HOME=/home/dbadescu/share/raglab_prod/hadoop/db-derby-10.14.2.0-bin
export PATH=$PATH:$DERBY_HOME/bin

#copy derbyclient.jar in hive lib/

#export CLASSPATH=$CLASSPATH:$DERBY_HOME/lib/*
#export HIVE_AUX_JARS_PATH=$DERBY_HOME/lib/*

export DERBY_OPTS="-Dderby.system.home=/home/dbadescu/share/raglab_prod/hadoop/derby_data"
#
export HADOOP_HOME=/home/dbadescu/share/raglab_prod/hadoop/hadoop-3.3.2
export PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin

export HIVE_HOME=/home/dbadescu/share/raglab_prod/hadoop/apache-hive-metastore-3.0.0-bin
export PATH=$PATH:$HIVE_HOME/bin
#export CLASSPATH=$CLASSPATH:$HIVE_HOME/lib/*
#initialize metastore schema.
#$HIVE_HOME/bin/schematool -dbType derby -initSchema --verbose


export SPARK_HOME=/home/dbadescu/share/raglab_prod/hadoop/spark-3.2.1-bin-hadoop3.2
export PATH=$PATH:$SPARK_HOME/sbin:$SPARK_HOME/bin

export TRINO_HOME=/home/dbadescu/share/raglab_prod/hadoop/trino-server-379
export PATH=$TRINO_HOME:$PATH


#export HADOOP_MAPRED_HOME=${HADOOP_HOME}
#export HADOOP_COMMON_HOME=${HADOOP_HOME}
#export HADOOP_HDFS_HOME=${HADOOP_HOME}
#export YARN_HOME=${HADOOP_HOME}

#
#export HADOOP_OPTS=-Djava.library.path=$root/lib/native
#setenv		HADOOP_COMMON_LIB_NATIVE_DIR	$root/lib/native
#setenv		HADOOP_OPTS			-Djava.library.path=$root/lib/native:$::env(LIBSNAPPY_HOME)/lib/libsnappy.so

#
# export CLASSPATH=$CLASSPATH:$HADOOP_HOME/lib/*:$HIVE_HOME/lib/*:$SPARK_HOME/lib/*
#export CLASSPATH=$CLASSPATH:$HADOOP_HOME/lib/*:$SPARK_HOME/lib/*
#
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HADOOP_HOME/lib/native:$HADOOP_HOME/lib
#export LIBRARY_PATH=$LIBRARY_PATH:$HADOOP_HOME/lib/native:$HADOOP_HOME/lib

#prepend-path    C_INCLUDE_PATH     $root/include
#prepend-path    CPLUS_INCLUDE_PATH $root/include
#prepend-path    PYTHONPATH 	   $::env(RAGLAB_INSTALL_HOME)/software/python/local-site-packages

