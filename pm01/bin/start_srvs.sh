#!/bin/bash

#cd /home/dbadescu/share/raglab_prod/hadoop
#. env_hadoop.sh

#start derby server
#java $DERBY_OPTS -jar $DERBY_HOME/lib/derbyrun.jar server start -noSecurityManager > derby.nohup.out 2>&1 &

# need create database when empty folder
#$HIVE_HOME/bin/schematool -dbType derby -initSchema --verbose

#start metastore server
#$HIVE_HOME/bin/start-metastore > metastore.nohup.out 2>&1 &

# start mongodb server, already initialized creating users
#$MONGODB_HOME/bin/mongod --auth --bind_ip_all --dbpath=$NASEQ_PROJ_DIR/servers/mongo_data > mongodb.nohup.out 2>&1 &

#start trino server
#nohup $TRINO_HOME/bin/launcher run > trino.nohup.out 2>&1 &


#$SPARK_HOME/sbin/start-master.sh
#$SPARK_HOME/sbin/start-worker.sh spark://cent77:7077 -c 4

#$SPARK_HOME/sbin/start-thriftserver.sh \
# --conf spark.cores.max=1 \
# --hiveconf hive.server2.thrift.min.worker.threads=1 \
# --hiveconf hive.server2.thrift.max.worker.threads=10 \
# --hiveconf hive.server2.thrift.port=10000 \
# --hiveconf hive.server2.transport.mode=http \
# --hiveconf hive.server2.thrift.http.port=10001 \
# --hiveconf hive.server2.http.endpoint=cliservice \
# --hiveconf hive.server2.thrift.bind.host=cent77 \
# --hiveconf hive.server2.authentication=NOSASL \
# --master spark://cent77:7077





